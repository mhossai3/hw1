## [Homework 1](https://gitlab.com/um-ece/courses/421/assignments/21sp/hw1)
### Embedded Systems
#### Spring 2021, Prof. Muldrey

In this assignment, you are going to setup a basic Linux environment and tools
which we will be using all semester. These tools include:

- Ubuntu 20.04 running in the Windows Subsystem for Linux 2 (WSL2)
- Git
- a Gitlab.com account


### Part 1: Gitlab.com account [1 pt]

- [ ] go to https://gitlab.com/users/sign_in and create an account (if you already have one, just login and skip this step)

- [ ] after you're logged in, navigate to https://gitlab.com/um-ece/courses/421/work/2021sp and click "Request Access" near the Project title.  This will give you write access to this area of Gitlab.com (once I approve)

- [ ] navigate to https://gitlab.com/um-ece/courses/421 and BOOKMARK IT in your browser :)




### Part 2: A suitable Linux terminal [1 pt]

- [ ] follow the instructions on the ECE HELP SITE (https://help.ece.olemiss.edu) for setting up the Windows Subsystem for Linux (you need WSL2) [DIRECT LINK](https://help.ece.olemiss.edu/system-tools/wsl2/)
 




### Part 3: Install 'Git' revision control tool into the Linux environment [1 pt]

- [ ] launch your Ubuntu terminal and at the prompt enter:
 
 ```bash
 sudo apt install git
 ```
 
 the OS will ask for your password, because `sudo ...` bascially means "do the following as an administrator"
 
 
 
 
### Part 4: Setup a key pair for using SSH

- [ ] launch your Ubuntu terminal and at the prompt enter:
 
 ```bash
 ssh-keygen
 ```
The OS will ask a lot of questions; **JUST HIT [enter] FOR ALL OF THEM**

- [ ] this creates a private RSA key and a corresponding public one.  The private one can be found at: `~/.ssh/id_rsa` and the public one is `~/.ssh/id_rsa.pub` **NEVER SHARE YOUR PUBLIC KEY FILE... EVER**

- [ ] print your **public** key to the screen:

```bash
cat ~/.ssh/id_rsa.pub
```

use your mouse and select the text that was output.  In most cases, you can use `<ctl><shift> c` to copy text from the terminal. If this doesn't work, google it.




### Part 5: Add your private key to your Gitlab.com account
so that you can use SSH from Linux to interact with repos at gitlab.com

- [ ] navigate to https://gitlab.com/-/profile/keys

- [ ] paste the contents of your public key file into the empty field.

- [ ] click "Add Key"

Now, whenever you clone a repository from the web to your local computer, you can use the "ssh"-style URL. They look like 'git<nolink>@gitlab.com:\<path\>/\<to\>\<repo\>'
 
 
 
 
### Part 6: Fork a copy of the Homework 1 Repository for yourself [3 pts]
 
- [ ] navigate to the 'assignments' folder on gitlab: https://gitlab.com/um-ece/courses/421/assignments/

- [ ] select '2021 Spring'

- [ ] click on 'homework 1'

- [ ] there is a little button in the top right that looks like this: ![](https://i.imgur.com/xGtkcrT.png)

- [ ] click it, and it will make a full copy of the 'homework 1' repository in your account on Github.com.

 
 
 
### Part 7: Clone your local repository to your machine [3 pts]

- [ ] go to your new 'fork' of the repo at https<nolink>://gitlab.com/\<YOUR-USERNAME-HERE\>/hw1

- [ ] click on "Clone" and click the little clipboard to the right of the SSH URL (this will copy the URL to your clipboard): 
![](https://i.imgur.com/JX6QIW7.png)

- [ ] in your Linux terminal, create a directory for this course:

```bash
cd ~
```
cd is for "change directory" -- it changes the working directory of the shell... i.e. "the place you are"

- [ ] make a new directory in your home:

```bash
mkdir ~/421_work
```

`~` is a shortcut for the user's home directory

- [ ] get inside your new directory:

```bash
cd ~/421_work
```

- [ ] clone a copy of YOUR fork of the homework repo:

```bash
git clone <path/to/your-fork-of-homework-1-repo>
```
(here you can use <ctl><shift>v to paste the url we copied above)

- [ ] get inside your local copy:

```bash
cd hw1
```

- [ ] get the status of your new repo:

```bash
git status
```

- [ ] take a screenshot of the output and save it somewhere.











### Part 8: Make a new Markdown file [10 pts]

- [ ] get inside your local repo again (if you left)

- [ ] use the "Nano" text editor to create and modify a new file:

```bash
nano submission.md
```

This will launch Nano, which is a text editor, and you will be editing a new file.

- [ ] Type the following into your file:

```nano
####  My name is <your name>

This is my submission for homework1. 
I would like to include one link: [name your link](url of your link)

I would also like to include some facts in a table:

|   Item        |          Info            |
|---------------|--------------------------|
| home town     | <your home town> 		 |
| favorite song | <your favorite song> 	 |
| <anything else> | <that you want to share with the class> |
```

- [ ] Type `<ctl>+x` to exit Nano.  It will ask if you want to save the file, type `y` for "yes"




### Part 9: Figure out how to locate your Ubuntu home from Windows [10pts]

Google is your friend

- [ ] I recommend saving a shortcut in Windows Explorer

- [ ] Copy or move the screenshot file you took earlier into the `421_work/hw1/` directory






### Part 10: Add your new files to Git and make a commit [15pts]

- [ ] get in your linux terminal and `cd` back into your `421_work/hw1` directory.

- [ ] check the status of your local repo
```bash
git status
```
it should show you that two files are currently untracked by Git.

- [ ] add the new files you created to Git:

```bash
git add -A
```
this tells Git to start tracking ALL files which are currently untracked

- [ ] Make a new commit:

```bash
git commit -am "working on homework"
```

This creates a new commit which includes ALL (-a) files which are "staged" (aka ready for committing) and will simultaneously set the commit message (-m) given.

Once this is done, all your modifications exist in your local repo.  That's great, but you also need to submit it so you can get your credit.

- [ ] Push all your local changes back to gitlab.com:

```bash
git push
```

This will push all your local commits to the default remote. When we used `git clone` above, the default remote was automatically set for us to the place we cloned from.

When you "forked" my "homework 1" repo, gitlab made a record.  Now, I can see a list of everyone who forked it, and visit that repo.  I will check your fork to make sure you completed the assignment.
